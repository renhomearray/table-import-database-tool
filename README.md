# 表格导入数据库工具

## 应用技术

- vue3
- tauri
- pinia

## 功能列表

- 支持导入 xlsl 文档
- 支持根据文档中的内容设置映射关系
- 支持导入映射关系
- 支持导出映射关系
- 支持导入单个字段的映射关系
- 支持预览结果
- 支持导出生成的sql
- 支持连续操作文档

## 效果展示

![image.png](document/image/image.png)

---

![image-1.png](document/image/image-1.png)

---

![image-2.png](document/image/image-2.png)

---

![image-3.png](document/image/image-3.png)

---

![image-4.png](document/image/image-4.png)

---

![image-5.png](document/image/image-5.png)

---

![image-6.png](document/image/image-6.png)

---

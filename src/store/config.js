import { defineStore } from "pinia"
import * as XLSX from "xlsx";
import createDiscreteApi from "@/utils/createDiscreteApi.js";

export const useConfigStore = defineStore("config", {
    state: () => ({
        /**
         * 设置的表数据库名
         */
        tableName: "",
        /**
         * 不同字段的转换关系
         */
        data: [],
    }),
    actions: {
        /**
         * 导入数据映射关系
         * @param {File} file 导入的文件
         * @param {Function} okFun 解析成功后处理策略
         * @returns 
         */
        importMap(file, okFun = (inputData) => { }) {
            let reader = new FileReader();
            reader.readAsArrayBuffer(file);
            reader.onload = function () {
                let buffer = reader.result
                let bytes = new Uint8Array(buffer)
                let length = bytes.byteLength
                let binary = ''
                for (let i = 0; i < length; i++) {
                    binary += String.fromCharCode(bytes[i])
                }
                let wb = XLSX.read(binary, {
                    type: 'binary'
                })
                // 读取表格中的数据,并转换格式
                let mapList = XLSX.utils.sheet_to_json(wb.Sheets[wb.SheetNames[0]])
                console.log(`读取文件${file.name}`, mapList);
                if (okFun && okFun instanceof Function) {
                    okFun(mapList);
                }
            }
        },
        /**
         * 导入已有配置文件
         * @param {File} file 导入的文件
         */
        importConfigOK(file) {
            var reader = new FileReader();
            reader.readAsText(file, "utf-8");
            var _this = this;
            reader.onload = function () {
                var fileData = JSON.parse(this.result);
                console.log(`读取文件${file.name}`, fileData);
                _this.tableName = fileData.tableName;
                let resultData = {};
                fileData.data.forEach(item => resultData[item.title] = item);
                _this.data = _this.data.map(item => {
                    let result = resultData[item.title];
                    if (result) {
                        result.values = item.values;
                        return result;
                    }
                    return item;
                });
            };
        },
        /**
         * 导出配置项
         */
        exportConfig() {
            var jsonData = {
                tableName: this.tableName,
                data: this.data.map(item => ({
                    field: item.field,
                    keyMap: item.keyMap,
                    map: item.map,
                    type: item.type,
                    code: item.code,
                    title: item.title
                }))
            };
            console.log("当前配置项", jsonData);
            function save(data) {
                const blob = new Blob([JSON.stringify(data)], { type: 'application/json' });
                const url = URL.createObjectURL(blob);
                const a = document.createElement('a');
                a.style.display = 'none';
                a.download = `${jsonData.tableName}_config.json`;
                a.href = url;
                document.body.appendChild(a);
                a.click();
                URL.revokeObjectURL(url);
                createDiscreteApi.message.info(`已导出当前配置项${a.download}`);
            }
            save(jsonData);
        },
    }
})
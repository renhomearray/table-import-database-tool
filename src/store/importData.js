import { defineStore } from "pinia"
import * as XLSX from "xlsx";
import { useConfigStore } from "./config";
export const useImportDataStore = defineStore("importData", {
    state: () => ({
        /**
         * 原始导入的数据信息
         */
        data: [],
        /**
         * 用于预览的数据信息
         */
        preData: [],
        /**
         * 原始导入的文件列表
         */
        fileList: []
    }),
    actions: {
        /**
         * 将原始导入的数据信息转换为用于预览的数据格式并返回
         * @param {boolean} isUpdata 是否刷新已有数据
         * @returns 用于预览的数据格式
         */
        getPreDataValue(isUpdata = false) {
            // 当要求强制刷新 或 原有预览数据为空 时 重新生成.
            if (isUpdata || this.preData || this.preData.length == 0) {
                const configStore = useConfigStore();
                this.preData = this.data.map(item => {
                    var result = {};
                    configStore.data.forEach(title => {
                        var map = title.map?.map;
                        if (map && Object.keys(map).length != 0) {
                            let a = map[item[title.title]];
                            result[title.title] = a ? a : item[title.title];
                        } else {
                            result[title.title] = item[title.title];
                        }
                    })
                    return result;
                })
            }
            return this.preData;
        },
        /**
         * 解析导入的数据信息
         * @param {File} file 导入的文件
         * @param {Function} okFn 文件解析后在该函数中返回结果
         * @returns 
         */
        input(file, okFn = (inputData) => { }) {
            if (!file) {
                if (okFn && !okFn instanceof Function) {
                    okFn([]);
                }
                return;
            }
            let reader = new FileReader();
            let _this = this;
            console.log("准备解析导入的文档", file.file);
            reader.readAsArrayBuffer(file.file);
            reader.onload = function () {
                let buffer = reader.result
                let bytes = new Uint8Array(buffer)
                let length = bytes.byteLength
                let binary = ''
                for (let i = 0; i < length; i++) {
                    binary += String.fromCharCode(bytes[i])
                }
                let wb = XLSX.read(binary, {
                    type: 'binary'
                })
                _this.data = XLSX.utils.sheet_to_json(wb.Sheets[wb.SheetNames[0]])
                console.log(`解析文件${file.name}`, _this.data);
                if (okFn && okFn instanceof Function) {
                    okFn(_this.data);
                }
            }
        }
    }
})
import { defineStore } from "pinia"
import { useConfigStore } from "./config"
import { useImportDataStore } from "@/store/importData.js"
export const useResultStore = defineStore("result", {
    state: () => ({
        /**
         * 转换完成的数据
         */
        resultData: [],
        /**
         * 最终生成的 sql 信息
         */
        resultSqlCode: '',
    }),
    actions: {
        /**
         * 获取最终转换生成的 sql 信息
         * @param {boolean} isUpdate 是否重新生成sql信息
         * @returns 最终转换生成的 sql 信息
         */
        getResultSqlCodeValue(isUpdate = false) {
            if (isUpdate || !this.resultSqlCode) {
                const configStore = useConfigStore();
                var importDataStore = useImportDataStore();
                var titleList = configStore.data.map(item => item.title);
                var fieldSql = configStore.data.map(item => '`' + item.field + '`').join(', ');
                var tableName = '`' + configStore.tableName + '`';
                this.resultSqlCode = importDataStore.getPreDataValue().map(item => {
                    // INSERT INTO users (name, age, email) VALUES ('Alice', 25, 'alice@example.com');
                    let value = titleList.map(title => item[title]).map(value => value ? `'${value}'` : 'NULL').join(', ');
                    return `INSERT INTO ${tableName} (${fieldSql}) VALUES (${value});`;
                }).join('\n');
            }
            return this.resultSqlCode;
        },
        /**
         * 导出 sql 结果
         */
        exportSql() {
            function save(data, tableName) {
                const blob = new Blob([data], { type: 'application/sql' });
                const url = URL.createObjectURL(blob);
                const a = document.createElement('a');
                a.style.display = 'none';
                a.download = `insert_${tableName}.sql`;
                a.href = url;
                document.body.appendChild(a);
                a.click();
                URL.revokeObjectURL(url);
            }
            const config = useConfigStore();
            save(this.resultSqlCode, config.tableName);
        }
    }
})
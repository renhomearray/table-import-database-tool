/**
 * 文档提示语
 */
const cueLanguage = `
/**
 * @param {Arrays} row 导入的原始数据的行数据
 * @returns 转换结果
 */
function format(row) {
  /* --- 案例数据 ------------------------
  row = [
    {"表头1":'第1行数据表头1的值', "表头2":"第1行数据表头2的值"},
    {"表头1":'第2行数据表头1的值', "表头2":"第2行数据表头2的值"}
  ]
  */
  // 在这里书写转换代码并返回转换结果
  return '非对象格式数据';
}
`;


export default {
    cueLanguage
}